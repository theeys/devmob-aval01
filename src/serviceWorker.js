
self.addEventListener('install', e => {
 e.waitUntil(
   caches.open('my-pwa-cache').then(cache => {
     return cache.addAll([
       'index.html',
       '404.html',
       'blank.html',
       'cards.html',
       'forgot-password.html',
       'login.html',
       'register.html',
       'tables.html',
       'utilities-animation.html',
       'utilities-border.html',
       'utilities-color.html',
       'utilities-other.html',
       'img/undraw_posting_photo.svg',
       'img/undraw_profile.svg',
       'img/undraw_profile_1.svg',
       'img/undraw_profile_2.svg',
       'img/undraw_profile_3.svg',
       'img/undraw_rocket.svg',
       'css/sb-admin-2.css',
       'css/sb-admin-2.min.css',
       'icons/android-chrome-192x192.png',
       'icons/android-chrome-512x512.png',
       'js/sb-admin-2.js',
       'js/sb-admin-2.min.js'
     ]);
   })
 );
});
